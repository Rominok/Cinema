import { connect } from 'react-redux';

// IMPORT YOUR REACT COMPONENT
import App from '../components/App';
// IMPORT YOUR ACTIONS
import demoActionCreator from '../actions/demoActionCreator';


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    demoActionCreator: () => {
      dispatch( demoActionCreator.fetch2() );
    },
    addData: () => {
      dispatch( demoActionCreator.doSomsng() );
    },
    doSomeSyncStuff: () => {
      // do some stuff
      dispatch({
        type: 'ACTION_TYPE',
        payload: ['some', 'data', 'sync']
      });
    }
  };
};

const MyApp = connect(
  mapDispatchToProps
)(App);

export default MyApp;
