import { connect } from 'react-redux';

// IMPORT YOUR REACT COMPONENT
import Test from '../components/test';
import demoActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (store, ownProps) => {
  return {
    ourData: store.testReducer.data
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    addData: ( array ) =>{
      dispatch( demoActionCreator.doSomsng(array) );
    },
    fetchData: () => {
      dispatch( demoActionCreator.fetchPromise() );
    }
  };
};

const TestComp = connect(
  mapStateToProps,
  mapDispatchToProps
)(Test);

export default TestComp;
