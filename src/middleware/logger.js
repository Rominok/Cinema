
/*
  applyMiddleware передает нам аргументы:
    store - redux-store нашего приложения;
    next - функция-обертка, которая позволяет продолжить выполнение цепочки;
    action - действе вызванное через store.dispatch
*/

// export const logger = store => next => action => {
//   console.log('myLogger', action);
//   return next(action);
// };
//
export const logger = function logger(store) {
  return function (next) {
    return function (action) {
      //
      console.log(action);

      return next(action);
    };
  };
};
