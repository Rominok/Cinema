import React, { Component } from 'react';

class Test extends Component {

  sendData = () => {
    this.props.addData([
      "one",
      "two",
      'three'
    ])
  }
  componentDidMount = () => {

    this.props.fetchData();
  }
  render = () => {
    let { sendData } = this;
    let { addData, ourData } = this.props;
    return(
      <div>
        <h1>Test</h1>
        <button onClick={sendData}>Add Data!</button>
        {
          ourData.length !== 0 ?
            ourData.map( (item, key) => (
              <div key={key}>{item}</div>
            )) : null
        }

      </div>)
  }
}

export default Test;
