import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Item = ({image, time, text, counter, url}) => (
  <Link to= {'/film/'+url} className='Item' >
    <figure>
      <img src={image}/>
    </figure>
    <div>
      <h1>{text}</h1>
      <h2>Кількість місць: {counter}</h2>
      <h2>Дата: {time}</h2>
    </div>
  </Link>
);

class Home extends Component {

  render() {
    let { data } = this.props;

    return (
      <div className="Home">
        <div className="headerHome">
          <h1>Розклад сеансів у Києві</h1>
          <h5>Оновлюється щосереди після 14:00 на один тиждень вперед, з
          четверга по наступну середу</h5>
        </div>
        <div>
          {
            data.data.map((item,key)=>(
              <Item
                key = {key}
                image = {item.image}
                time = {item.time}
                text = {item.text}
                counter = {item.counter}
                title = {item.title}
                url = {key}
              />
            ))
          }
        </div>
      </div>
    );
  }
}
// - - - - - - - -  -
const MapStateToProps = (state, ownProps) => {
  return {
    data: state.filmData
  }
}


const ConnectedHome = connect(
  MapStateToProps
)(Home)

export default ConnectedHome;
